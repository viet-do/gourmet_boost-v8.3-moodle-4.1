Moodle 4.1 Theme Gourmet Boost
=======================================================================
Gourmet Boost is a responsive Moodle theme designed for online learning.

Theme name:
=======================================================================
Gourmet Boost

Theme version:
=======================================================================
v8.3

Release Date:
=======================================================================
2023-07-19

Author: 
=======================================================================
3rd Wave Media (https://elearning.3rdwavemedia.com/)

Licenses: 
=======================================================================
https://elearning.3rdwavemedia.com/licenses/

Moodle version required:
=======================================================================
Moodle 4.1

Theme Parents
=======================================================================
Moodle Boost Theme

Installation:
=======================================================================
1) Download the theme zip file
2) Extract the /gourmet_boost/ folder and the files.
3) Upload the /gourmet_boost/ folder to your hosting server's Moodle theme directory:
   Your Moodle installation > theme > gourmet_boost 
4) Log into your Moodle site as an admin, and navigate to the theme selector:
   Settings > Site administration > Appearance > Themes > Theme selector
5) Click "use theme" next to the Gourmet Boost theme to activate

Support:
=======================================================================
Without prior agreement, the price of the theme does not include additional support or consultancy.

Contact:
=======================================================================
Web: https://elearning.3rdwavemedia.com/
Email: elearning@3rdwavemedia.com
Twitter: @3rdwave_moodle
